package simulation;

import banking.Message;
import banking.Balances;
import banking.Card;
import banking.Money;

/**
 * @author dll
 * @version 1.0
 * @created 30-11��-2021 7:17:18
 */
public class SimulatedBankTest extends junit.framework.TestCase {

	public SimulatedBankTest(){

	}

	public void finalize() throws Throwable {
		super.finalize();
	}
	/**
	 * 
	 * @param arg0
	 */
	public SimulatedBankTest(String arg0){
		super(arg0);
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args){

	}

	/**
	 * 
	 * @exception Exception
	 */
	protected void setUp()
	  throws Exception{
		super.setUp();
	}

	/**
	 * 
	 * @exception Exception
	 */
	protected void tearDown()
	  throws Exception{
		super.tearDown();
	}

	//TUC01 card=-1
	public final void testHandleMessage_cardneg1(){
		Message message = new Message(0, new Card(-1), 42, 1, 1, 2,  new Money(0,20)); 
		SimulatedBank sb =new SimulatedBank();
		Balances balances=new Balances();
		assertEquals("FAILURE Invalid card", sb.handleMessage(message,balances).toString());
	}
	//TUC02 card=0
	public final void testHandleMessage_card0(){
		Message message = new Message(0, new Card(0), 42, 1, 1, 2,  new Money(0,20)); 
		SimulatedBank sb =new SimulatedBank();
		Balances balances=new Balances();
		assertEquals("FAILURE Invalid card", sb.handleMessage(message,balances).toString());
	}
	//TUC03 card=1
	public final void testHandleMessage_card1(){
		Message message = new Message(0, new Card(1), 42, 1, 1, 2,  new Money(0,20)); 
		SimulatedBank sb =new SimulatedBank();
		Balances balances=new Balances();
		assertEquals("SUCCESS", sb.handleMessage(message,balances).toString());
	}
	//TUC04 card=1
	public final void testHandleMessage_card2(){
		Message message = new Message(0, new Card(43), 42, 1, 1, 2,  new Money(0,20)); 
		SimulatedBank sb =new SimulatedBank();
		Balances balances=new Balances();
		assertEquals("FAILURE Invalid card", sb.handleMessage(message,balances).toString());
	}
	//TUC05 card=2
	public final void testHandleMessage_card2_1234(){
		Message message = new Message(0, new Card(2), 1234, 1, 1, 2,  new Money(0,20)); 
		SimulatedBank sb =new SimulatedBank();
		Balances balances=new Balances();
		assertEquals("FAILURE Invalid account type", sb.handleMessage(message,balances).toString());
	}
	//TUC06 card=3
	//java.lang.ArrayIndexOutOfBoundsException: 3
	//at simulation.SimulatedBank.handleMessage(SimulatedBank.java:33)
	//at simulation.SimulatedBankTest.testHandleMessage_card3_1234(SimulatedBankTest.java:96)
	//if (cardNumber < 1 || cardNumber > PIN.length)
	//if (cardNumber < 1 || cardNumber >= PIN.length)
	public final void testHandleMessage_card3_1234(){
		Message message = new Message(0, new Card(3), 1234, 1, 1, 2,  new Money(0,20)); 
		SimulatedBank sb =new SimulatedBank();
		Balances balances=new Balances();
		assertEquals("FAILURE Invalid card", sb.handleMessage(message,balances).toString());
	}
	//TUC07 card=1
	public final void testHandleMessage_3_card1_42_1_1(){
		Message message = new Message(3, new Card(1), 42, 1, 1, 1,  new Money(0,20)); 
		SimulatedBank sb =new SimulatedBank();
		Balances balances=new Balances();
		assertEquals("FAILURE Can't transfer money from\n" +
                    "an account to itself", sb.handleMessage(message,balances).toString());
	}
	//TUC08 card=1
	public final void testHandleMessage_3_card1_42_1_2(){
		Message message = new Message(3, new Card(1), 42, 1, 0, 1,  new Money(0,20)); 
		SimulatedBank sb =new SimulatedBank();
		Balances balances=new Balances();
		assertEquals("SUCCESS", sb.handleMessage(message,balances).toString());
	}
	
	//TUC09 card=1  IT
	/*
	public final void testHandleMessage_withdrawal_0_1_42_1_1_2_300(){
		Message message = new Message(0, new Card(1), 42, 1, 1, 2,  new Money(300,0)); 
		SimulatedBank sb =new SimulatedBank();
		Balances balances=new Balances();
		assertEquals("SUCCESS", sb.handleMessage(message,balances).toString());
	}
	*/
	//TUC10 card=1  IT
	/*
	public final void testHandleMessage_withdrawal_0_1_42_1_1_2_301(){
		Message message = new Message(0, new Card(1), 42, 1, 1, 2,  new Money(300,20)); 
		SimulatedBank sb =new SimulatedBank();
		Balances balances=new Balances();
		assertEquals("FAILURE Daily withdrawal limit exceeded", sb.handleMessage(message,balances).toString());
	}*/
}//end SimulatedBankTest